import {MetricsPanelCtrl} from 'app/plugins/sdk';
import _ from 'lodash';
import rendering from './rendering';

export class ParallelCtrl extends MetricsPanelCtrl {

  constructor($scope, $injector, $rootScope, $interpolate, $sanitize, templateSrv, detangleSrv) {
    super($scope, $injector);
    this.$rootScope = $rootScope;
    this.$interpolate = $interpolate;
    this.$sanitize = $sanitize;
    this.templateSrv = templateSrv;
    this.detangleSrv = detangleSrv;
    this.parcoords;
    var panelDefaults = {
      detangle: {
        coupling: false,
        maintainabilityIndex: false,
        diamondPattern: false,
        qualityEffort: false,
        applyFolderLevel: false,
        metric: 'coupling',
        target: 'file',
        cohesionCalculationMethod: 'standard',
        sourceType: '$issue_type',
        targetType: '$target_issue_type',
        sourceTypeData: '',
        targetTypeData: '',
        author: '$author',
        authorData: '',
        yearData: '',
        minIssuesPerFile: '$min_issues',
        minIssuesData: '',
        minFilesPerIssue: null,
        minFilesData: '',
        issueTitle: '$issue_title',
        issueTitleData: '',
        fileExcludeFilter: '$file_exclude',
        fileExcludeFilterData: '',
        metricRange: '$metric_range',
        metricRangeData: '',
        fileGroup: '$file_group',
        threshold: 0
      }
    };

    _.defaults(this.panel, panelDefaults);
    this.panel.parallelDivId = 'd3parallel_svg_' + this.panel.id;
    //this.containerDivId = 'container_'+this.panel.parallelDivId;

    //this.events.on('render', this.onRender.bind(this));
    this.events.on('data-received', this.onDataReceived.bind(this));
    this.events.on('data-error', this.onDataError.bind(this));
    this.events.on('data-snapshot-load', this.onDataReceived.bind(this));
    this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
    this.sortingOrder = [{ text: 'Ascending', value: 'asc' }, { text: 'Descending', value: 'desc' }];

    this.couplingMetrics = [
      {text: 'Coupling Value', value: 'coupling'},
      {text: 'Num. of Couples', value: 'couplecounts'},
      {text: 'Cohesion Value', value: 'cohesion'},

    ];

    this.targetSelections = [
      {text: 'Issues|Committers', value: 'issue'},
      {text: 'Files', value: 'file'},
    ];

    this.cohesionCalculationMethods = [
      {
        text: 'Standard', value: 'standard'
      },
      {
        text: 'Double', value: 'double'
      }
    ];
  }

  onInitEditMode() {
    this.addEditorTab('Detangle', 'public/plugins/grafana-treemap-panel/detangle.html', 2);
  }


  onDataError() {
    this.data = [];
    this.render();
  }

  setContainer(container) {
    this.panelContainer = container;
    this.panel.svgContainer = container;
  }


  colorSelectOptions(){
    var values = ["index","regular expression"];

    if(!this.columns)
      return[];

    var selectors = _.map(this.columns,"text");

    selectors.splice(-1);

    return values.concat(selectors);
  }


  onDataReceived(dataList) {
    let data = dataList[0];

    if(!data)
    {
      this._error = "No data points.";
      return this.render();
    }

    if(data.type !== "table")
    {
      this._error = "Should be table fetch. Use terms only.";
      return this.render();
    }
    this.panel.detangle.applyFolderLevel = true;
    let constant = this.detangleSrv.dataConvertor([dataList[0]], this.templateSrv, this.panel.detangle);
    let newDataList = [];
    newDataList.push(constant[0]);
    let constant2 = this.detangleSrv.dataConvertor([dataList[4]], this.templateSrv, this.panel.detangle);
    newDataList.push(constant2[0]);
    let qeeiConfig = _.clone(this.panel.detangle);
    qeeiConfig.qualityEffort = true;
    let qeei = this.detangleSrv.dataConvertor([dataList[1], dataList[2]], this.templateSrv, qeeiConfig);
    qeei[0].columns = qeei[0].columns.slice(0, 2);
    qeei[0].rows = qeei[0].rows.map(item => {
      return { 0: item[0], 1: item[1] };
    });
    newDataList.push(qeei[0]);
    let newConfig = _.clone(this.panel.detangle);
    newConfig.coupling = true;
    newConfig.maintainabilityIndex = true;
    newConfig.diamondPattern = true;
    let couplingResult = this.detangleSrv.dataConvertor(dataList.slice(3), this.templateSrv, newConfig, 'table');
    newDataList.push(couplingResult[0]);
    data = newDataList;

    console.log(data);
    this._error = null;
    let globalTarget = this.templateSrv.replaceWithText('$global_target', this.panel.scopedVars);
    if (globalTarget) {
      globalTarget = globalTarget.trim();
    }
    let shouldApplyGlobalTarget = globalTarget !== "" && globalTarget !== '-' && globalTarget !== '$global_target';
    if (shouldApplyGlobalTarget) {
      this.panel.detangle.target = globalTarget === 'files' ? 'file' : 'issue';
    }
    this.data = data;
    console.log(this.data);
    this.render(this.data);
  }

  downloadScreenshot() {
    var callback = function (canvas) {
      // Download the image
      var download = document.createElement("a");
      download.href = canvas.toDataURL("image/png");
      download.download = "parallel-coordinate.png";
      download.click();
    };
    this.parcoords.mergeParcoords(callback);
  }

  link(scope, elem, attrs, ctrl) {
    rendering(scope, elem, attrs, ctrl);
  }


  highlight(){
    this.render();
  }
}

ParallelCtrl.templateUrl = 'module.html';
