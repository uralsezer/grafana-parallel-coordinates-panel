'use strict';

System.register(['app/plugins/sdk', 'lodash', './rendering'], function (_export, _context) {
  "use strict";

  var MetricsPanelCtrl, _, rendering, _createClass, ParallelCtrl;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_appPluginsSdk) {
      MetricsPanelCtrl = _appPluginsSdk.MetricsPanelCtrl;
    }, function (_lodash) {
      _ = _lodash.default;
    }, function (_rendering) {
      rendering = _rendering.default;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export('ParallelCtrl', ParallelCtrl = function (_MetricsPanelCtrl) {
        _inherits(ParallelCtrl, _MetricsPanelCtrl);

        function ParallelCtrl($scope, $injector, $rootScope, $interpolate, $sanitize, templateSrv, detangleSrv) {
          _classCallCheck(this, ParallelCtrl);

          var _this = _possibleConstructorReturn(this, (ParallelCtrl.__proto__ || Object.getPrototypeOf(ParallelCtrl)).call(this, $scope, $injector));

          _this.$rootScope = $rootScope;
          _this.$interpolate = $interpolate;
          _this.$sanitize = $sanitize;
          _this.templateSrv = templateSrv;
          _this.detangleSrv = detangleSrv;
          _this.parcoords;
          var panelDefaults = {
            detangle: {
              coupling: false,
              maintainabilityIndex: false,
              diamondPattern: false,
              qualityEffort: false,
              applyFolderLevel: false,
              metric: 'coupling',
              target: 'file',
              cohesionCalculationMethod: 'standard',
              sourceType: '$issue_type',
              targetType: '$target_issue_type',
              sourceTypeData: '',
              targetTypeData: '',
              author: '$author',
              authorData: '',
              yearData: '',
              minIssuesPerFile: '$min_issues',
              minIssuesData: '',
              minFilesPerIssue: null,
              minFilesData: '',
              issueTitle: '$issue_title',
              issueTitleData: '',
              fileExcludeFilter: '$file_exclude',
              fileExcludeFilterData: '',
              metricRange: '$metric_range',
              metricRangeData: '',
              fileGroup: '$file_group',
              threshold: 0
            }
          };

          _.defaults(_this.panel, panelDefaults);
          _this.panel.parallelDivId = 'd3parallel_svg_' + _this.panel.id;
          //this.containerDivId = 'container_'+this.panel.parallelDivId;

          //this.events.on('render', this.onRender.bind(this));
          _this.events.on('data-received', _this.onDataReceived.bind(_this));
          _this.events.on('data-error', _this.onDataError.bind(_this));
          _this.events.on('data-snapshot-load', _this.onDataReceived.bind(_this));
          _this.events.on('init-edit-mode', _this.onInitEditMode.bind(_this));
          _this.sortingOrder = [{ text: 'Ascending', value: 'asc' }, { text: 'Descending', value: 'desc' }];

          _this.couplingMetrics = [{ text: 'Coupling Value', value: 'coupling' }, { text: 'Num. of Couples', value: 'couplecounts' }, { text: 'Cohesion Value', value: 'cohesion' }];

          _this.targetSelections = [{ text: 'Issues|Committers', value: 'issue' }, { text: 'Files', value: 'file' }];

          _this.cohesionCalculationMethods = [{
            text: 'Standard', value: 'standard'
          }, {
            text: 'Double', value: 'double'
          }];
          return _this;
        }

        _createClass(ParallelCtrl, [{
          key: 'onInitEditMode',
          value: function onInitEditMode() {
            this.addEditorTab('Detangle', 'public/plugins/grafana-treemap-panel/detangle.html', 2);
          }
        }, {
          key: 'onDataError',
          value: function onDataError() {
            this.data = [];
            this.render();
          }
        }, {
          key: 'setContainer',
          value: function setContainer(container) {
            this.panelContainer = container;
            this.panel.svgContainer = container;
          }
        }, {
          key: 'colorSelectOptions',
          value: function colorSelectOptions() {
            var values = ["index", "regular expression"];

            if (!this.columns) return [];

            var selectors = _.map(this.columns, "text");

            selectors.splice(-1);

            return values.concat(selectors);
          }
        }, {
          key: 'onDataReceived',
          value: function onDataReceived(dataList) {
            var data = dataList[0];

            if (!data) {
              this._error = "No data points.";
              return this.render();
            }

            if (data.type !== "table") {
              this._error = "Should be table fetch. Use terms only.";
              return this.render();
            }
            this.panel.detangle.applyFolderLevel = true;
            var constant = this.detangleSrv.dataConvertor([dataList[0]], this.templateSrv, this.panel.detangle);
            var newDataList = [];
            newDataList.push(constant[0]);
            var constant2 = this.detangleSrv.dataConvertor([dataList[4]], this.templateSrv, this.panel.detangle);
            newDataList.push(constant2[0]);
            var qeeiConfig = _.clone(this.panel.detangle);
            qeeiConfig.qualityEffort = true;
            var qeei = this.detangleSrv.dataConvertor([dataList[1], dataList[2]], this.templateSrv, qeeiConfig);
            qeei[0].columns = qeei[0].columns.slice(0, 2);
            qeei[0].rows = qeei[0].rows.map(function (item) {
              return { 0: item[0], 1: item[1] };
            });
            newDataList.push(qeei[0]);
            var newConfig = _.clone(this.panel.detangle);
            newConfig.coupling = true;
            newConfig.maintainabilityIndex = true;
            newConfig.diamondPattern = true;
            var couplingResult = this.detangleSrv.dataConvertor(dataList.slice(3), this.templateSrv, newConfig, 'table');
            newDataList.push(couplingResult[0]);
            data = newDataList;

            console.log(data);
            this._error = null;
            var globalTarget = this.templateSrv.replaceWithText('$global_target', this.panel.scopedVars);
            if (globalTarget) {
              globalTarget = globalTarget.trim();
            }
            var shouldApplyGlobalTarget = globalTarget !== "" && globalTarget !== '-' && globalTarget !== '$global_target';
            if (shouldApplyGlobalTarget) {
              this.panel.detangle.target = globalTarget === 'files' ? 'file' : 'issue';
            }
            this.data = data;
            console.log(this.data);
            this.render(this.data);
          }
        }, {
          key: 'downloadScreenshot',
          value: function downloadScreenshot() {
            var callback = function callback(canvas) {
              // Download the image
              var download = document.createElement("a");
              download.href = canvas.toDataURL("image/png");
              download.download = "parallel-coordinate.png";
              download.click();
            };
            this.parcoords.mergeParcoords(callback);
          }
        }, {
          key: 'link',
          value: function link(scope, elem, attrs, ctrl) {
            rendering(scope, elem, attrs, ctrl);
          }
        }, {
          key: 'highlight',
          value: function highlight() {
            this.render();
          }
        }]);

        return ParallelCtrl;
      }(MetricsPanelCtrl));

      _export('ParallelCtrl', ParallelCtrl);

      ParallelCtrl.templateUrl = 'module.html';
    }
  };
});
//# sourceMappingURL=parallel_ctrl.js.map
