'use strict';

System.register(['./parallel_ctrl'], function (_export, _context) {
  "use strict";

  var ParallelCtrl;
  return {
    setters: [function (_parallel_ctrl) {
      ParallelCtrl = _parallel_ctrl.ParallelCtrl;
    }],
    execute: function () {
      _export('PanelCtrl', ParallelCtrl);
    }
  };
});
//# sourceMappingURL=module.js.map
